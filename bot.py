import logging
import os
from aiogram import Bot, Dispatcher, executor, types
from utils import load_json, save_json
from weatherapi import forecast


users_db_file = 'users.json'
users = load_json(users_db_file) or {}
bot = Bot(token=os.getenv('TELEGRAM_TEST_BOT_TOKEN'))
dispatcher = Dispatcher(bot)


logging.basicConfig(level=logging.INFO)


@dispatcher.message_handler(commands=['start', 'help'])
async def cmd_help(message: types.Message):
    msg = """Приветствую Вас, о мой господин!\nЯ, InfoBot ибн nornad, слушаю Вас и повинуюсь.
    <u>Что я умею</u>:
    <b>/start</b> - <i>this help page</i>
    <b>/help</b> - <i>this help page</i>
    <b>/locate</b> - <i>share your location</i>
    <b>/weather</b> - <i>check current weather in your location</i>
    """
    await message.answer(msg, parse_mode='HTML')


def location_keyboard():
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    button = types.KeyboardButton("Share GEO", request_location=True)
    keyboard.add(button)
    return keyboard


async def get_forecast(username: str) -> str:
    data = await forecast(apikey=os.getenv('WEATHERAPI_KEY'),
                          location=users[username]['location'], lang='ru')

    location = data['location']['name']
    if data['location']['country']:
        location = f"{location} ({data['location']['country']})"

    wind_dirs = {'N': 'северный', 'NW': 'северо-западный', 'NE': 'северо-восточный',
                 'W': 'западный',
                 'E': 'восточный', 'S': 'южный', 'SW': 'юго-западный', 'SE': 'юго-восточный',
                 'NNE': 'северо-северо-восточный', 'NNW': 'северо-северо-западный',
                 'SSE': 'юго-юго-восточный', 'SSW': 'юго-юго-западный',
                 'ENE': 'восточно-северо-восточный', 'ESE': 'восточно-юго-восточный',
                 'WNW': 'западно-северо-западный', 'WSW': 'западно-юго-западный', }
    wind_dir = wind_dirs.get(data['current']['wind_dir'], data['current']['wind_dir'])
    wind_kph = data['current']['wind_kph']
    wind_mps = round(wind_kph / 3.6, 1)

    return f"Погода в <i>{location}</i> на {data['location']['localtime']}" \
           f" (получена в {data['current']['last_updated']})\n" \
           f"<b>{data['current']['temp_c']}°C</b>, Ветер {wind_dir}," \
           f" {wind_kph} км/ч ({wind_mps} м/с)," \
           f" <b>{data['current']['condition']['text']}</b>"


@dispatcher.message_handler(commands='weather')
async def cmd_weather(message: types.Message):
    if message.from_user.username not in users:
        users[message.from_user.username] = {'show_weather': True,
                                             'user_id': message.from_user.id}
        await message.answer("Нажмите кнопку ниже, чтобы поделиться местоположением",
                             reply_markup=location_keyboard())
    else:
        await message.answer(await get_forecast(message.from_user.username), parse_mode='HTML')


@dispatcher.message_handler(commands='locate')
async def cmd_locate(message: types.Message):
    await message.answer("Нажмите кнопку ниже, чтобы поделиться местоположением, о мой господин",
                         reply_markup=location_keyboard())


@dispatcher.message_handler(content_types='location')
async def handle_location(message: types.Message):

    users[message.from_user.username]['location'] = f"{message.location.latitude}," \
                                                    f"{message.location.longitude}"
    users[message.from_user.username]['user_id'] = message.from_user.id

    await message.answer('Спасибо, о Великий!', reply_markup=types.ReplyKeyboardRemove())

    if users[message.from_user.username].get('show_weather'):
        del users[message.from_user.username]['show_weather']
        await message.answer(await get_forecast(message.from_user.username), parse_mode='HTML')

    save_json(users_db_file, users)


if __name__ == '__main__':
    # skip_updates ставим, если нам не нужны апдейты, пропущенные пока бот не работал
    executor.start_polling(dispatcher, skip_updates=True)
