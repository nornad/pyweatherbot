import json
import os


def load_json(filepath):
    if not os.path.exists(filepath):
        return None
    with open(filepath) as f:
        return json.load(f)


def save_json(filepath, data, indent: int = 4, sort_keys: bool = True):
    # unsafe!!!
    with open(filepath, 'w') as f:
        json.dump(data, f, indent=indent, sort_keys=sort_keys)
